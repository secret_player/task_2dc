#FROM python:3.6
FROM python:3.6-alpine3.10

WORKDIR /src
COPY ./requirements.txt .
#RUN pip3 install -r requirements.txt && rm -rf /var/cache/apt
RUN \
 apk add --no-cache postgresql-client postgresql-libs && \
 apk add --no-cache --virtual build-deps gcc musl-dev postgresql-dev && \
 python3 -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del build-deps

COPY . .
ENV FLASK_APP=app.py
#ENV POSTGRESQL_URL=postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$HOST_DB/$POSTGRES_DB
ENV POSTGRESQL_URL=postgresql://worker:worker@db:5432/app
EXPOSE 5000
ENTRYPOINT ["./entrypoint.sh"]